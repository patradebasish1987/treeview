use clap::{Arg, Command};
use colored::Colorize;
use std::fs;
use std::path::{Path, PathBuf};

fn main() {

    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .author("Debasish Patra")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Tree View For A Directory")
        .arg(
            Arg::new("path")
                .short('p')
                .long("path")
                .required(false)
                .help("Provide Path for Tree View"),
        )
        .arg(
            Arg::new("depth")
                .short('d')
                .long("depth")
                .required(false)
                .help("Depth of Tree View"),
        )
        .arg(
            Arg::new("showhidden")
                .short('h')
                .long("showhiddenfiles")
                .action(clap::ArgAction::SetTrue)
                .required(false)
                .help("Show Hidden Files"),
        )
        .arg(
            Arg::new("showdirsonly")
                .short('D')
                .long("showdirsonly")
                .action(clap::ArgAction::SetTrue)
                .required(false)
                .help("Show Directories Only"),
        )
        .get_matches();
    //println!("{}", args[1]);

    let newpath = matches
        .get_one::<String>("path")
        .map(String::as_str)
        .unwrap_or("./");
    let maxlevel = matches
        .get_one::<String>("depth")
        .map(String::as_str)
        .unwrap_or("3")
        .parse::<u32>()
        .unwrap();
    let showhiddenfiles = matches.get_one::<bool>("showhidden").unwrap();
    let showdirsonly = matches.get_one::<bool>("showdirsonly").unwrap();
    let mut showfile = true;
    if *showdirsonly {
        showfile = false;
    }

    let res = fs::canonicalize(PathBuf::from(&newpath));

    let mut path: PathBuf = fs::canonicalize(PathBuf::from("./")).unwrap();
    if res.is_ok() {
        path = fs::canonicalize(PathBuf::from(newpath)).unwrap();
    } else {
        println!("Invalid Path Provided : {}", newpath);
        return;
    }

    let path = Path::new(&path);
    print_folder_structure(&path, 0, maxlevel, showhiddenfiles, showfile);
}
fn print_folder_structure(
    path: &Path,
    depth: u32,
    maxlevel: u32,
    showhiddenfiles: &bool,
    showfile: bool,
) {
    let mut folderchar =String::from("▼");
    let paths = fs::read_dir(path.to_str().unwrap().to_string());
    if depth == maxlevel{
    folderchar = String::from("▶");
    }
    println!(
        "{}",
        format!(
            "{} {}",
            folderchar.cyan(),
            path.file_name().unwrap().to_str().unwrap().to_string()
        )
    );
    if depth >= maxlevel {
        //println!("Depth : {}", depth);
        return;
    }

    for each_path in paths.unwrap() {
        if (!*showhiddenfiles
            && !each_path
                .as_ref()
                .unwrap()
                .path()
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .starts_with("."))
            || *showhiddenfiles
        {
            if each_path.as_ref().unwrap().path().is_dir() {
                //if each_path.as_ref().unwrap().path().is_dir(){
                for _ in 0..depth {
                    print!("\t");
                }
                print!("\t└── ");
                //println!("{}", Path::new(each_path.unwrap().path()));
                print_folder_structure(
                    Path::new(&each_path.as_ref().unwrap().path()),
                    depth + 1,
                    maxlevel,
                    showhiddenfiles,
                    showfile,
                );
            } else {
                // Print the space required first
                //if *showfilesonly && each_path.as_ref().unwrap().path().is_file(){
                if showfile {
                    for _ in 0..depth {
                        print!("\t");
                    }
                    print!("\t└── ");

                    println!(
                        "{}",
                        format!(
                            "{} {}",
                            "●".green(),
                            each_path
                                .unwrap()
                                .path()
                                .file_name()
                                .unwrap()
                                .to_str()
                                .unwrap()
                        )
                    );
                }
            }
        }
    }
}
