# Treeview



## About

Presenting TreeView project which shows the folder and files in a treeview.

## Usage Snapshot

``` console
binary git:main ❯❯❯ ./treeview -p ..                                                                                                                                                                                                       ⏎ ✹
🖿  treeview
	└── 🖿  binary
		└── ●  treeview
	└── ●  Cargo.toml
	└── 🖿  src
		└── ●  main.rs
	└── ●  README.md
binary git:main ❯❯❯     
```

## Usage Help
``` console
binary git:main ❯❯❯ ./treeview --help                                                                                                                                                                                                        ✹
Tree View For A Directory

Usage: treeview [OPTIONS]

Options:
  -p, --path <path>      Provide Path for Tree View
  -d, --depth <depth>    Depth of Tree View
  -h, --showhiddenfiles  Show Hidden Files
  -D, --showdirsonly     Show Directories Only
  -h, --help             Print help information
  -V, --version          Print version information
binary git:main ❯❯❯    
```

## Authors and acknowledgment
Debasish Patra

## License
GPL-2
